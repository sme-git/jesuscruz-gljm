connection: "mybqtets"

# include all the views
include: "*.view"

datagroup: gitlab_project_default_datagroup {
  # sql_trigger: SELECT MAX(id) FROM etl_log;;
  max_cache_age: "1 hour"
}

persist_with: gitlab_project_default_datagroup

explore: municipal_sf_requests {}

explore: neighborhood_pd {}

explore: neighborhood_zip {}

explore: sffd_service_calls {}

explore: sfpd_incidents {}

explore: zipcode_neighborhood_grp {}
